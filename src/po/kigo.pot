# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KMediaFactory
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: damu@iki.fi\n"
"POT-Creation-Date: 2015-05-05 12:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: main.cpp:41
msgid "Kigo"
msgstr ""

#: main.cpp:42
msgid "KDE Go Board Game"
msgstr ""

#: main.cpp:43
msgid "Copyright (c) 2008-2010 Sascha Peilicke"
msgstr ""

#: main.cpp:44
msgid "Sascha Peilicke (saschpe)"
msgstr ""

#: main.cpp:44
msgid "Original author"
msgstr ""

#: main.cpp:46
msgid "Florent Descroix (hen-sen)"
msgstr ""

#: main.cpp:46 main.cpp:48
msgid "Additionnal author"
msgstr ""

#: main.cpp:48
msgid "Damien Moulard"
msgstr ""

#: main.cpp:50
msgid "Yuri Chornoivan"
msgstr ""

#: main.cpp:50
msgid "Documentation editor"
msgstr ""

#: main.cpp:52
msgid "Arturo Silva"
msgstr ""

#: main.cpp:52
msgid "Default theme designer"
msgstr ""

#: main.cpp:59
msgctxt "@info:shell"
msgid "Game to load (SGF file)"
msgstr ""

#: game/game.cpp:327
#, kde-format
msgctxt "%1 stone coordinate"
msgid "White %1"
msgstr ""

#: game/game.cpp:330 game/game.cpp:400 gui/mainwindow.cpp:385
msgid "White passed"
msgstr ""

#: game/game.cpp:336
#, kde-format
msgctxt "%1 stone coordinate"
msgid "Black %1"
msgstr ""

#: game/game.cpp:339 game/game.cpp:402 gui/mainwindow.cpp:387
msgid "Black passed"
msgstr ""

#: game/game.cpp:409
msgid "White resigned"
msgstr ""

#: game/game.cpp:411
msgid "Black resigned"
msgstr ""

#: game/game.cpp:419
#, kde-format
msgctxt "%1 response from Go engine"
msgid "White %1"
msgstr ""

#: game/game.cpp:421
#, kde-format
msgctxt "%1 response from Go engine"
msgid "Black %1"
msgstr ""

#. i18n: file: gui/kigoui.rc:6
#. i18n: ectx: Menu (game)
#: rc.cpp:3 rc.cpp:3
msgctxt "@title:menu"
msgid "&Game"
msgstr ""

#. i18n: file: gui/kigoui.rc:17
#. i18n: ectx: Menu (move)
#: rc.cpp:6 rc.cpp:6
msgctxt "@title:menu"
msgid "&Move"
msgstr ""

#. i18n: file: gui/kigoui.rc:22
#. i18n: ectx: Menu (settings)
#: rc.cpp:9 rc.cpp:9
msgctxt "@title:menu"
msgid "&Settings"
msgstr ""

#. i18n: file: gui/kigoui.rc:24
#. i18n: ectx: Menu (dockers)
#: rc.cpp:12 rc.cpp:12
msgctxt "@title:menu"
msgid "&Dockers"
msgstr ""

#. i18n: file: gui/kigoui.rc:34
#. i18n: ectx: ToolBar (mainToolBar)
#: rc.cpp:15 rc.cpp:15
msgctxt "@title:menu"
msgid "Main Toolbar"
msgstr ""

#. i18n: file: gui/kigoui.rc:40
#. i18n: ectx: ToolBar (moveToolBar)
#: rc.cpp:18 rc.cpp:18
msgctxt "@title:menu"
msgid "Move Toolbar"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:17
#. i18n: ectx: property (title), widget (QGroupBox, backendGroupBox)
#: rc.cpp:21 rc.cpp:21
msgid "Backend"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:23
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: rc.cpp:24 rc.cpp:24
msgid "Executable:"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:42
#. i18n: ectx: property (toolTip), widget (KUrlRequester, engineExecutable)
#: rc.cpp:27 rc.cpp:27
msgid "Select the executable file to start the Go engine"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:49
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:30 rc.cpp:30
msgid "Parameters:"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:62
#. i18n: ectx: property (toolTip), widget (KLineEdit, engineParameters)
#: rc.cpp:33 rc.cpp:33
msgid "Add here the necessary parameters to start the Engine in GTP mode"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:75
#. i18n: ectx: property (toolTip), widget (KLineEdit, kcfg_EngineCommand)
#: rc.cpp:36 rc.cpp:36
msgid "This is the resulting engine command which will be used by Kigo"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:97
#. i18n: ectx: property (toolTip), widget (KLed, engineLed)
#: rc.cpp:39 rc.cpp:39
msgid "Indicates whether the Go engine works correctly"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:111
#. i18n: ectx: property (text), widget (QLabel, label_4)
#: rc.cpp:42 rc.cpp:42
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Please select a Go engine "
"that supports the <span style=\" font-style:italic;\">GnuGo Text Protocol "
"(GTP)</span>. The indicator light turns green when the selected backend is "
"working.</p></body></html>"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:131
#. i18n: ectx: property (title), widget (QGroupBox, appearanceGroupBox)
#: rc.cpp:49 rc.cpp:49
msgid "Appearance"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:140
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_ShowBoardLabels)
#: rc.cpp:52 rc.cpp:52
msgid "Display Board Labels"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:147
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: rc.cpp:55 rc.cpp:55
msgid "Hint Visibility Time:"
msgstr ""

#. i18n: file: gui/config/generalconfig.ui:166
#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_HintVisibleTime)
#: rc.cpp:58 rc.cpp:58
msgid " Seconds"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:17
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:61 rc.cpp:61
msgid "Player:"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:120
#. i18n: ectx: property (text), widget (QLabel, label_4)
#. i18n: file: gui/widgets/analysiswidget.ui:146
#. i18n: ectx: property (text), widget (QLabel, label_8)
#. i18n: file: gui/widgets/gamewidget.ui:191
#. i18n: ectx: property (text), widget (QLabel, whiteCapturesLabel)
#. i18n: file: gui/widgets/gamewidget.ui:265
#. i18n: ectx: property (text), widget (QLabel, blackCapturesLabel)
#. i18n: file: gui/widgets/analysiswidget.ui:120
#. i18n: ectx: property (text), widget (QLabel, label_4)
#. i18n: file: gui/widgets/analysiswidget.ui:146
#. i18n: ectx: property (text), widget (QLabel, label_8)
#. i18n: file: gui/widgets/gamewidget.ui:191
#. i18n: ectx: property (text), widget (QLabel, whiteCapturesLabel)
#. i18n: file: gui/widgets/gamewidget.ui:265
#. i18n: ectx: property (text), widget (QLabel, blackCapturesLabel)
#: rc.cpp:64 rc.cpp:70 rc.cpp:216 rc.cpp:222 rc.cpp:64 rc.cpp:70 rc.cpp:216
#: rc.cpp:222
msgid "Captures:"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:133
#. i18n: ectx: property (text), widget (QLabel, label_6)
#. i18n: file: gui/widgets/analysiswidget.ui:159
#. i18n: ectx: property (text), widget (QLabel, label_9)
#. i18n: file: gui/widgets/analysiswidget.ui:133
#. i18n: ectx: property (text), widget (QLabel, label_6)
#. i18n: file: gui/widgets/analysiswidget.ui:159
#. i18n: ectx: property (text), widget (QLabel, label_9)
#: rc.cpp:67 rc.cpp:73 rc.cpp:67 rc.cpp:73
msgid "Estimated points:"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:184
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: rc.cpp:76 rc.cpp:76
msgid "Tools:"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:193
#. i18n: ectx: property (text), widget (QRadioButton, radioButton_2)
#: rc.cpp:79 rc.cpp:79
msgid "&Remove pawn"
msgstr ""

#. i18n: file: gui/widgets/analysiswidget.ui:216
#. i18n: ectx: property (text), widget (QRadioButton, radioButton)
#: rc.cpp:82 rc.cpp:82
msgid "&Add pawn"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:30
#. i18n: ectx: property (text), widget (QLabel, label_12)
#: rc.cpp:85 rc.cpp:85
msgid "Board size:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:37
#. i18n: ectx: property (text), widget (QRadioButton, sizeSmall)
#: rc.cpp:88 rc.cpp:88
msgid "Tiny (&9x9)"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:47
#. i18n: ectx: property (text), widget (QRadioButton, sizeMedium)
#: rc.cpp:91 rc.cpp:91
msgid "Small (&13x13)"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:54
#. i18n: ectx: property (text), widget (QRadioButton, sizeBig)
#: rc.cpp:94 rc.cpp:94
msgid "No&rmal (19x19)"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:66
#. i18n: ectx: property (text), widget (QRadioButton, sizeOther)
#: rc.cpp:97 rc.cpp:97
msgid "Custom:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:103
#. i18n: ectx: property (text), widget (QLabel, label_5)
#: rc.cpp:100 rc.cpp:100
msgid "&Handicap:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:126
#. i18n: ectx: property (toolTip), widget (KIntSpinBox, handicapSpinBox)
#: rc.cpp:103 rc.cpp:103
msgid ""
"Handicap stones are an advantage for the black player.\n"
"Black gets a number of stones on the board before the game starts."
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:129
#. i18n: ectx: property (specialValueText), widget (KIntSpinBox, handicapSpinBox)
#: rc.cpp:107 rc.cpp:107
msgid "No handicap"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:132
#. i18n: ectx: property (suffix), widget (KIntSpinBox, handicapSpinBox)
#: rc.cpp:110 rc.cpp:110
msgid " Stones"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:154
#. i18n: ectx: property (text), widget (QLabel, label_4)
#: rc.cpp:113 rc.cpp:113
msgid "&Komi:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:174
#. i18n: ectx: property (toolTip), widget (QDoubleSpinBox, komiSpinBox)
#: rc.cpp:116 rc.cpp:116
msgid ""
"Komi are points given to the white player at the end of the game.\n"
"It balances out the advantage black has by making the first move."
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:177
#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, komiSpinBox)
#: rc.cpp:120 rc.cpp:120
msgid " Points"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:240
#. i18n: ectx: property (text), widget (QLabel, eventStaticLabel)
#: rc.cpp:123 rc.cpp:123
msgid "Event:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:275
#. i18n: ectx: property (text), widget (QLabel, locationStaticLabel)
#: rc.cpp:126 rc.cpp:126
msgid "Location:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:310
#. i18n: ectx: property (text), widget (QLabel, dateStaticLabel)
#: rc.cpp:129 rc.cpp:129
msgid "Date:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:342
#. i18n: ectx: property (text), widget (QLabel, roundStaticLabel)
#: rc.cpp:132 rc.cpp:132
msgid "Round:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:374
#. i18n: ectx: property (text), widget (QLabel, scoreStaticLabel)
#: rc.cpp:135 rc.cpp:135
msgid "Score:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:406
#. i18n: ectx: property (text), widget (QLabel, komiStaticLabel)
#. i18n: file: gui/widgets/gamewidget.ui:42
#. i18n: ectx: property (text), widget (QLabel, label_3)
#. i18n: file: gui/widgets/setupwidget.ui:406
#. i18n: ectx: property (text), widget (QLabel, komiStaticLabel)
#. i18n: file: gui/widgets/gamewidget.ui:42
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: rc.cpp:138 rc.cpp:201 rc.cpp:138 rc.cpp:201
msgid "Komi:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:438
#. i18n: ectx: property (text), widget (QLabel, continueStaticLabel)
#: rc.cpp:141 rc.cpp:141
msgid "Continue:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:451
#. i18n: ectx: property (text), widget (QLabel, playerLabel)
#: rc.cpp:144 gui/widgets/setupwidget.cpp:237 rc.cpp:144
msgid "Black to play"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:470
#. i18n: ectx: property (suffix), widget (KIntSpinBox, startMoveSpinBox)
#: rc.cpp:147 rc.cpp:147
msgid " of 999"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:473
#. i18n: ectx: property (prefix), widget (KIntSpinBox, startMoveSpinBox)
#: rc.cpp:150 rc.cpp:150
msgid "Move "
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:492
#. i18n: ectx: property (text), widget (QLabel, timeStaticLabel)
#: rc.cpp:153 rc.cpp:153
msgid "Time limit:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:575
#. i18n: ectx: property (text), widget (QCheckBox, whiteIsComputerCheckBox)
#: rc.cpp:156 rc.cpp:156
msgid "&Computer"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:598
#. i18n: ectx: property (text), widget (QLabel, label_6)
#. i18n: file: gui/widgets/setupwidget.ui:765
#. i18n: ectx: property (text), widget (QLabel, label_7)
#. i18n: file: gui/widgets/setupwidget.ui:598
#. i18n: ectx: property (text), widget (QLabel, label_6)
#. i18n: file: gui/widgets/setupwidget.ui:765
#. i18n: ectx: property (text), widget (QLabel, label_7)
#: rc.cpp:159 rc.cpp:171 rc.cpp:159 rc.cpp:171
msgid "Name:"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:640
#. i18n: ectx: property (text), widget (QLabel, label1)
#: rc.cpp:162 rc.cpp:162
msgid "Wea&k"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:659
#. i18n: ectx: property (text), widget (QLabel, label2)
#: rc.cpp:165 rc.cpp:165
msgid "St&rong"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:745
#. i18n: ectx: property (text), widget (QCheckBox, blackIsComputerCheckBox)
#: rc.cpp:168 rc.cpp:168
msgid "C&omputer"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:807
#. i18n: ectx: property (text), widget (QLabel, label2_3)
#: rc.cpp:174 rc.cpp:174
msgid "Strong"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:826
#. i18n: ectx: property (text), widget (QLabel, label1_3)
#: rc.cpp:177 rc.cpp:177
msgid "Weak"
msgstr ""

#. i18n: file: gui/widgets/setupwidget.ui:1006
#. i18n: ectx: property (text), widget (KPushButton, startButton)
#: rc.cpp:180 rc.cpp:180
msgid "Start Game"
msgstr ""

#. i18n: file: gui/widgets/errorwidget.ui:21
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:183 rc.cpp:183
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:8pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"family:'Sans Serif'; font-size:10pt; color:#ff0000;\">Kigo was unable to "
"find a  Go engine backend.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; "
"margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-"
"family:'Sans Serif'; font-size:10pt; color:#ff0000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"family:'Sans Serif'; font-size:10pt;\">If you are sure that you already "
"installed a suitable Go engine, you might want to configure Kigo to use that "
"engine. Otherwise you should install a Go engine (like GnuGo).</span></p></"
"body></html>"
msgstr ""

#. i18n: file: gui/widgets/errorwidget.ui:49
#. i18n: ectx: property (text), widget (KPushButton, configureButton)
#: rc.cpp:192 rc.cpp:192
msgid "Configure Kigo..."
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:19
#. i18n: ectx: property (text), widget (QLabel, label_5)
#: rc.cpp:195 rc.cpp:195
msgid "Handicap:"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:35
#. i18n: ectx: property (text), widget (QLabel, handicapLabel)
#: rc.cpp:198 rc.cpp:198
msgid "2"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:58
#. i18n: ectx: property (text), widget (QLabel, komiLabel)
#: rc.cpp:204 rc.cpp:204
msgid "0.5 Points"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:78
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:207 rc.cpp:207
msgid "Last move:"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:101
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: rc.cpp:210 rc.cpp:210
msgid "Move:"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:184
#. i18n: ectx: property (text), widget (QLabel, whiteNameLabel)
#: rc.cpp:213 rc.cpp:213
msgid "White Player"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:233
#. i18n: ectx: property (text), widget (QLabel, blackNameLabel)
#: rc.cpp:219 rc.cpp:219
msgid "Black Player"
msgstr ""

#. i18n: file: gui/widgets/gamewidget.ui:308
#. i18n: ectx: property (text), widget (KPushButton, finishButton)
#: rc.cpp:225 rc.cpp:225
msgid "Finish Game"
msgstr ""

#. i18n: file: kigo.kcfg:9
#. i18n: ectx: label, entry (EngineList), group (Backend)
#: rc.cpp:228 rc.cpp:228
msgid "List of available/configured engine backends"
msgstr ""

#. i18n: file: kigo.kcfg:16
#. i18n: ectx: label, entry (EngineCommands), group (Backend)
#: rc.cpp:231 rc.cpp:231
msgid "List of backend commands corresponding to the engine list"
msgstr ""

#. i18n: file: kigo.kcfg:23
#. i18n: ectx: label, entry (EngineCommand), group (Backend)
#: rc.cpp:234 rc.cpp:234
msgid "The current game engine command with (optional) parameters"
msgstr ""

#. i18n: file: kigo.kcfg:32
#. i18n: ectx: label, entry (Theme), group (UserInterface)
#: rc.cpp:237 rc.cpp:237
msgid "The graphical theme to be used"
msgstr ""

#. i18n: file: kigo.kcfg:36
#. i18n: ectx: label, entry (ShowBoardLabels), group (UserInterface)
#: rc.cpp:240 rc.cpp:240
msgid "Determines whether board labels are shown"
msgstr ""

#. i18n: file: kigo.kcfg:40
#. i18n: ectx: label, entry (ShowMoveNumbers), group (UserInterface)
#: rc.cpp:243 rc.cpp:243
msgid "Move numbers are drawn onto stones if enabled"
msgstr ""

#. i18n: file: kigo.kcfg:44
#. i18n: ectx: label, entry (HintVisibleTime), group (UserInterface)
#: rc.cpp:246 rc.cpp:246
msgid "Number of seconds for which a hint is visible"
msgstr ""

#. i18n: file: kigo.kcfg:52
#. i18n: ectx: label, entry (BlackPlayerHuman), group (Game)
#: rc.cpp:249 rc.cpp:249
msgid "Is black a human player?"
msgstr ""

#. i18n: file: kigo.kcfg:56
#. i18n: ectx: label, entry (BlackPlayerName), group (Game)
#: rc.cpp:252 rc.cpp:252
msgid "The name of the black player"
msgstr ""

#. i18n: file: kigo.kcfg:60
#. i18n: ectx: label, entry (BlackPlayerStrength), group (Game)
#: rc.cpp:255 rc.cpp:255
msgid "The strength of the black player"
msgstr ""

#. i18n: file: kigo.kcfg:66
#. i18n: ectx: label, entry (WhitePlayerHuman), group (Game)
#: rc.cpp:258 rc.cpp:258
msgid "Is white a human player?"
msgstr ""

#. i18n: file: kigo.kcfg:70
#. i18n: ectx: label, entry (WhitePlayerName), group (Game)
#: rc.cpp:261 rc.cpp:261
msgid "The name of the white player"
msgstr ""

#. i18n: file: kigo.kcfg:74
#. i18n: ectx: label, entry (WhitePlayerStrength), group (Game)
#: rc.cpp:264 rc.cpp:264
msgid "The strength of the white player"
msgstr ""

#. i18n: file: kigo.kcfg:80
#. i18n: ectx: label, entry (BoardSize), group (Game)
#: rc.cpp:267 rc.cpp:267
msgid "Go board size"
msgstr ""

#. i18n: file: kigo.kcfg:87
#. i18n: ectx: label, entry (Komi), group (Game)
#: rc.cpp:270 rc.cpp:270
msgid "Komi"
msgstr ""

#. i18n: file: kigo.kcfg:88
#. i18n: ectx: tooltip, entry (Komi), group (Game)
#: rc.cpp:273 rc.cpp:273
msgid "With komi you can give the black player some extra points"
msgstr ""

#. i18n: file: kigo.kcfg:94
#. i18n: ectx: label, entry (FixedHandicapValue), group (Game)
#: rc.cpp:276 rc.cpp:276
msgid "Extra stones for the black player"
msgstr ""

#: gui/mainwindow.cpp:114
msgid "Set up a new game..."
msgstr ""

#: gui/mainwindow.cpp:131
msgid "Set up a loaded game..."
msgstr ""

#: gui/mainwindow.cpp:134
msgid "Unable to load game..."
msgstr ""

#: gui/mainwindow.cpp:187
msgid "Game saved..."
msgstr ""

#: gui/mainwindow.cpp:189
msgid "Unable to save game."
msgstr ""

#: gui/mainwindow.cpp:237
msgid "Game started..."
msgstr ""

#: gui/mainwindow.cpp:269
#, kde-format
msgid "%1 won with a score of %2."
msgstr ""

#: gui/mainwindow.cpp:272
#, kde-format
msgid "%1 won with a score of %2 (bounds: %3 and %4)."
msgstr ""

#: gui/mainwindow.cpp:282
msgid "Undone move"
msgstr ""

#: gui/mainwindow.cpp:292
msgid "Redone move"
msgstr ""

#: gui/mainwindow.cpp:317
msgid "General"
msgstr ""

#: gui/mainwindow.cpp:318
msgid "Themes"
msgstr ""

#: gui/mainwindow.cpp:335
msgid "Backend was changed, restart necessary..."
msgstr ""

#: gui/mainwindow.cpp:397 gui/mainwindow.cpp:399
msgctxt "@action"
msgid "Get More Games..."
msgstr ""

#: gui/mainwindow.cpp:405 gui/mainwindow.cpp:407
msgctxt "@action"
msgid "Start Game"
msgstr ""

#: gui/mainwindow.cpp:411 gui/mainwindow.cpp:413
msgctxt "@action"
msgid "Finish Game"
msgstr ""

#: gui/mainwindow.cpp:421
msgctxt "@action:inmenu Move"
msgid "Pass Move"
msgstr ""

#: gui/mainwindow.cpp:426
msgctxt "@action:inmenu View"
msgid "Show Move &Numbers"
msgstr ""

#: gui/mainwindow.cpp:439
msgctxt "@title:window"
msgid "Game Setup"
msgstr ""

#: gui/mainwindow.cpp:451 gui/mainwindow.cpp:457
msgctxt "@title:window"
msgid "Information"
msgstr ""

#: gui/mainwindow.cpp:463 gui/mainwindow.cpp:472
msgctxt "@title:window"
msgid "Moves"
msgstr ""

#: gui/mainwindow.cpp:466
msgid "No move"
msgstr ""

#: gui/mainwindow.cpp:479
msgctxt "@title:window"
msgid "Analysis"
msgstr ""

#: gui/mainwindow.cpp:485
msgctxt "@title:window"
msgid "Analyze"
msgstr ""

#: gui/mainwindow.cpp:491
msgctxt "@title:window"
msgid "Error"
msgstr ""

#: gui/widgets/setupwidget.cpp:64
msgid " Stone"
msgid_plural " Stones"
msgstr[0] ""
msgstr[1] ""

#: gui/widgets/setupwidget.cpp:168
#, kde-format
msgctxt "Time limit of a game in minutes"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: gui/widgets/setupwidget.cpp:170
#, kde-format
msgctxt "Time limit of a game, hours, minutes"
msgid "%1 hour, %2"
msgid_plural "%1 hours, %2"
msgstr[0] ""
msgstr[1] ""

#: gui/widgets/setupwidget.cpp:192
#, kde-format
msgid " of %1"
msgstr ""

#: gui/widgets/setupwidget.cpp:235
msgid "White to play"
msgstr ""

#: gui/widgets/gamewidget.cpp:56 gui/widgets/gamewidget.cpp:61
#, kde-format
msgid "Computer (level %1)"
msgstr ""

#: gui/widgets/gamewidget.cpp:64
#, kde-format
msgid "%1 Stone"
msgid_plural "%1 Stones"
msgstr[0] ""
msgstr[1] ""

#: gui/widgets/gamewidget.cpp:81
#, kde-format
msgctxt "Indication who played the last move"
msgid "%1 (white)"
msgstr ""

#: gui/widgets/gamewidget.cpp:83
#, kde-format
msgctxt "Indication who played the last move"
msgid "%1 (black)"
msgstr ""

#: gui/widgets/gamewidget.cpp:92
msgid "White's turn"
msgstr ""

#: gui/widgets/gamewidget.cpp:96
msgid "Black's turn"
msgstr ""

#: gui/widgets/gamewidget.cpp:101 gui/widgets/gamewidget.cpp:102
#, kde-format
msgid "%1 capture"
msgid_plural "%1 captures"
msgstr[0] ""
msgstr[1] ""
