/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef KIGO_ANALYSEWIDGET_H
#define KIGO_ANALYSEWIDGET_H

#include "ui_analysiswidget.h"

#include <QWidget>

namespace Kigo {

class Game;

class AnalysisWidget : public QWidget, private Ui::AnalysisWidget
{
		Q_OBJECT

	public:
		explicit AnalysisWidget(Game *game, QWidget *parent = 0);
		~AnalysisWidget();

    signals:
        void leaveAnalysisClicked();

	private:
		Game *m_game;

    private slots:
        void init();
        void launchAnalysisButtonClicked();
        void playerChanged();
        void analysisToolChanged();
};

} // End of namespace Kigo

#endif // ANALYSEWIDGET_H

