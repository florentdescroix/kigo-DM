/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "analysiswidget.h"
#include "game/game.h"
#include "preferences.h"

#include "gui/graphicsview/themerenderer.h"

namespace Kigo {

AnalysisWidget::AnalysisWidget(Game *game, QWidget *parent)
	: QWidget(parent), m_game(game)
{
	Q_ASSERT(m_game);
    setupUi(this);

	QPixmap whiteStone = ThemeRenderer::self()->renderElement(Kigo::ThemeRenderer::WhiteStone, QSize(48, 48));
    whiteStoneImageLabel->setPixmap(whiteStone);
	QPixmap blackStone = ThemeRenderer::self()->renderElement(Kigo::ThemeRenderer::BlackStone, QSize(48, 48));
    blackStoneImageLabel->setPixmap(blackStone);

    connect(launchAnalysisButton, SIGNAL(clicked()), this, SLOT(launchAnalysisButtonClicked()));
    connect(leaveAnalysisButton, SIGNAL(clicked()), this, SIGNAL(leaveAnalysisClicked()));
    connect(whitePlayer, SIGNAL(clicked(bool)), this, SLOT(playerChanged()));
    connect(blackPlayer, SIGNAL(clicked(bool)), this, SLOT(playerChanged()));
    connect(addOnePawn, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
    connect(addMultiplePawns, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
    connect(removePawn, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
}

AnalysisWidget::~AnalysisWidget()
{

}

void AnalysisWidget::init()
{
    if (m_game->currentPlayer().isBlack()) {
        blackPlayer->setChecked(true);
    } else if (m_game->currentPlayer().isWhite()) {
        whitePlayer->setChecked(true);
    }
}

void AnalysisWidget::launchAnalysisButtonClicked()
{

}

void AnalysisWidget::playerChanged()
{
    if(whitePlayer->isChecked()) {
        m_game->setCurrentPlayer(m_game->whitePlayer());
    } else if (blackPlayer->isChecked()) {
        m_game->setCurrentPlayer(m_game->blackPlayer());
    }

}

void AnalysisWidget::analysisToolChanged()
{
    if(addOnePawn->isChecked()) {
        m_game->setcurrentAnalysisTool(m_game->AddOnePawn);
    } else if (addMultiplePawns->isChecked()) {
        kDebug() <<"blblbl";
        m_game->setcurrentAnalysisTool(m_game->AddMultiplePawns);
    } else if (removePawn->isChecked()) {
        m_game->setcurrentAnalysisTool(m_game->RemovePawn);
        kDebug() <<"pout";
    }
}

}  // End of namespace Kigo

#include "moc_analysiswidget.cpp"
